package com.kenfogel.javafx_03_forms02.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Ken Fogel
 */
public class UserBean {

    private StringProperty userName;
    private StringProperty userPassword;

    /**
     * Non-default constructor
     *
     * @param userName
     * @param userPassword
     */
    public UserBean(final String userName, final String userPassword) {
        super();
        this.userName = new SimpleStringProperty(userName);
        this.userPassword = new SimpleStringProperty(userPassword);
    }

    /**
     * Default Constructor
     */
    public UserBean() {
        this("", "");
    }

    public String getUserName() {
        return userName.get();
    }

    public void setUserName(String userName) {
        this.userName.set(userName);
    }

    public StringProperty userNameProperty() {
        return userName;
    }

    public String getUserPassword() {
        return userPassword.get();
    }

    public void setUserPassword(String userPassword) {
        this.userPassword.set(userPassword);
    }

    public StringProperty userPasswordProperty() {
        return userPassword;
    }

    public String toString() {
        return "Name: " + userName.get() + "  Password: " + userPassword.get();
    }

}
