package com.kenfogel.javafx_03_forms02.presentation;

import com.kenfogel.javafx_03_forms02.data.UserBean;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Example of a form created with code and using CSS for styling
 *
 * @author Ken
 */
public class Form02GUI {

    private TextField userNameTextField;
    private PasswordField userPasswordTextField;
    private Text actionTarget;

    private final UserBean userBean;

    /**
     * Constructor that assigns the reference for the UserBean passed from the
     * MainApp and calls initialize to bind the bean to specific controls
     *
     * @param userBean
     */
    public Form02GUI(UserBean userBean) {
        this.userBean = userBean;
        initialize();
    }

    /**
     * The stage and the scene are created in the start.
     *
     * @param primaryStage
     */
    public void start(Stage primaryStage) {
        // Set Window's Title
        primaryStage.setTitle("JavaFX Form 02");
        GridPane root = createUserInterface();
        Scene scene = new Scene(root, 300, 275);
        scene.getStylesheets().add(getClass().getResource("/styles/Login.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Create the user interface as the root
     *
     * @return
     */
    private GridPane createUserInterface() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text scenetitle = new Text("Welcome");
        scenetitle.setId("welcome-text");
        grid.add(scenetitle, 0, 0, 2, 1);

        final Label userName = new Label("User Name:");
        grid.add(userName, 0, 1);

        grid.add(userNameTextField, 1, 1);

        Label pw = new Label("Password:");
        grid.add(pw, 0, 2);

        grid.add(userPasswordTextField, 1, 2);

        Button btn = new Button("Sign in");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btn);
        grid.add(hbBtn, 1, 4);

        actionTarget.setId("actiontarget");
        grid.add(actionTarget, 0, 6, 2, 1);

        btn.setOnAction(this::signInButtonHandler);

        return grid;
    }

    /**
     * Event handler for the Sign In Button
     *
     * @param e
     */
    private void signInButtonHandler(ActionEvent e) {
        actionTarget.setText(userBean.toString());
    }

    /**
     * This code binds the properties of the data bean to the JavaFX controls.
     * Changes to a control is immediately written to the bean and a change to
     * the bean is immediately shown in the control.
     */
    private void initialize() {
        userNameTextField = new TextField();
        userPasswordTextField = new PasswordField();
        actionTarget = new Text();
        Bindings.bindBidirectional(userNameTextField.textProperty(), userBean.userNameProperty());
        Bindings.bindBidirectional(userPasswordTextField.textProperty(), userBean.userPasswordProperty());
    }
}
